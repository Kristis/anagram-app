<?php

use Phinx\Migration\AbstractMigration;

class Dictionary extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $table = $this->table('dictionaries');
        $table
            ->addColumn('name', 'string')
            ->create();

        $table = $this->table('dic_words');
        $table
            ->addColumn('dictionary_id', 'integer')
            ->addForeignKey('dictionary_id', 'dictionaries', 'id', ['delete'=> 'RESTRICT', 'update'=> 'NO_ACTION'])
            ->update();

        $data = [
            'id' => 1,
            'name'  => 'default'
        ];

        $table = $this->table('dictionaries');
        $table->insert($data);
        $table->saveData();

        $data = [
            [
                'word'  => 'amor',
                'dictionary_id' => 1
            ],
            [
                'word'  => 'coro',
                'dictionary_id' => 1
            ],
            [
                'word'  => 'cosa',
                'dictionary_id' => 1
            ],
            [
                'word'  => 'mora',
                'dictionary_id' => 1
            ],
            [
                'word'  => 'roma',
                'dictionary_id' => 1
            ],
            [
                'word'  => 'ramo',
                'dictionary_id' => 1
            ],
            [
                'word'  => 'saco',
                'dictionary_id' => 1
            ],
        ];

        for($i = 0; $i < count($data); $i++){
            $wordLetters = str_split($data[$i]['word']);
            sort($wordLetters);
            $data[$i]['signature'] = implode('',$wordLetters);
        }

        $table = $this->table('dic_words');
        $table->insert($data);
        $table->saveData();

    }
}
