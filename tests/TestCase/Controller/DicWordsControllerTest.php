<?php
namespace App\Test\TestCase\Controller;

use App\Controller\DicWordsController;
use Cake\TestSuite\IntegrationTestCase;

/**
 * App\Controller\DicWordsController Test Case
 */
class DicWordsControllerTest extends IntegrationTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.dic_words'
    ];

    public function testIndex()
    {
        $this->get('/api/dic/1/words');

        $this->assertResponseOk();
        $this->assertResponseContains('words');
        $this->assertResponseContains('amor');
        $this->assertResponseContains('coro');

        $this->get('/api/dic/0/words');

        $this->assertResponseOk();
        $this->assertResponseContains('words');
        $this->assertResponseNotContains("id");
    }

    /**
     * Test view method
     *
     * @return void
     */
    public function testView()
    {
        $this->get('/api/words/view/2');
        $this->assertResponseOk();
        $this->assertResponseContains('word');
        $this->assertResponseContains('coor');
    }

    /**
     * Test add method
     *
     * @return void
     */
    public function testAdd()
    {
        $data = [
            'word' => "fedcba",
            'dictionary_id' => 1
        ];
        $this->post('/api/dic/1/words/add', $data);

        $this->assertResponseOk();
        $this->assertResponseContains('fedcba');
        $this->assertResponseContains('abcdef');

        $data = [
            'word' => "amor"
        ];
        $this->post('/api/dic/1/words/add', $data);

        $this->assertResponseOk();
        $this->assertResponseContains('error');
    }

    /**
     * Test edit method
     *
     * @return void
     */
    public function testEdit()
    {
        $data = [
            'word' => "fedcba"
        ];
        $this->post('/api/words/edit/1', $data);

        $this->assertResponseOk();
        $this->assertResponseContains('fedcba');
        $this->assertResponseContains('abcdef');

        $data = [
            'word' => "coro"
        ];
        $this->post('/api/words/edit/1', $data);

        $this->assertResponseOk();
        $this->assertResponseContains('error');

    }

    /**
     * Test delete method
     *
     * @return void
     */
    public function testDelete()
    {
        $this->delete('/api/words/delete/1');

        $this->assertResponseOk();
        $this->assertResponseContains('success');
    }

    public function testAnagram()
    {
        $this->get('/api/dic/1/anagrams?words=amor+coro+cosa+mora+roma+ramo+saco');

        $this->assertResponseOk();
        $this->assertResponseContains('dictionary');
    }

}
