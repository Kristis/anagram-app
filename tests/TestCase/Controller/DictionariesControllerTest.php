<?php
namespace App\Test\TestCase\Controller;

use App\Controller\DictionariesController;
use Cake\TestSuite\IntegrationTestCase;

/**
 * App\Controller\DictionariesController Test Case
 */
class DictionariesControllerTest extends IntegrationTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.dictionaries',
        'app.dic_words'
    ];

    /**
     * Test index method
     *
     * @return void
     */
    public function testIndex()
    {
        $this->get('/api/dictionaries');
        $this->assertResponseOk();
        $this->assertResponseContains('dictionaries');
    }

    /**
     * Test view method
     *
     * @return void
     */
    public function testView()
    {
        $this->get('/api/dictionaries/view/1');
        $this->assertResponseOk();
        $this->assertResponseContains('dictionary');
        $this->assertResponseContains('default');
    }

    /**
     * Test add method
     *
     * @return void
     */
    public function testAdd()
    {
        $data = [
            'name' => "test_dictionary_name"
        ];
        $this->post('/api/dictionaries/add', $data);

        $this->assertResponseOk();
        $this->assertResponseContains('test_dictionary_name');
    }

    /**
     * Test edit method
     *
     * @return void
     */
    public function testEdit()
    {
        $data = [
            'name' => "test_dictionary_name"
        ];
        $this->post('/api/dictionaries/edit/1', $data);

        $this->assertResponseOk();
        $this->assertResponseContains('test_dictionary_name');
    }

    /**
     * Test delete method
     *
     * @return void
     */
    public function testDelete()
    {
        $this->delete('/api/dictionaries/delete/1');

        $this->assertResponseOk();
        $this->assertResponseContains('success');
    }
}
