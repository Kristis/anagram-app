<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\DicWordsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\DicWordsTable Test Case
 */
class DicWordsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\DicWordsTable
     */
    public $DicWords;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.dic_words'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('DicWords') ? [] : ['className' => 'App\Model\Table\DicWordsTable'];
        $this->DicWords = TableRegistry::get('DicWords', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->DicWords);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
