## Configuration

By default this application requires two databases to be created: `anagram_app` and `anagram_app_test` . 
Database connection should allow these user credentials: username - superAdmin; password - asdasd.

If above requirements can't be met it is possible to change the configuration in `config/app.php`

When database connections are configured run these commands in the root folder of the application:
```bash
composer update
bin\cake migrations migrate

```

## REST API usage

DICTIONARIES:

```bash
api/dictionaries
allowed methods: all
parameters: none
returns: list of all dictionaries
```

```bash
api/dictionaries/add
allowed methods: post
parameters: 'name' - name of new dictionary
returns: newly created dictionary
```

```bash
api/dictionaries/view/{dictionary_id}
allowed methods: All
parameters: none
returns: dictionary entity
```

```bash
api/dictionaries/edit/{dictionary_id}
allowed methods: patch, post, put
parameters: 'name','signature','dictionary_id' - new name for dictionary (optional)
returns: updated dictionary entity
```

```bash
api/dictionaries/delete/{dictionary_id}
allowed methods: post, delete
parameters: none
returns: success message if entity was deleted
```

WORDS:

```bash
api/dic/{dictionary_id}/anagrams
allowed methods: All
parameters: words (as url parameter)
returns: list of anagrams for each provided word in selected dictionary
```

```bash
api/dic/{dictionary_id}/words
allowed methods: All
parameters: none
returns: list of all words in specified dictionary
```

```bash
api/dic/{dictionary_id}/words/add
allowed methods: post
parameters: 'word' - word to be added to dictionary
returns: newly created word
```

```bash
api/words/view/{word_id}
allowed methods: All
parameters: none
returns: word entity (contains word signature)
```

```bash
api/words/edit/{word_id}
allowed methods: patch, post, put
parameters: 'word','signature','dictionary_id' - new values for word entity (all optional)
returns: updated word entity
```

```bash
api/words/delete/{word_id}
allowed methods: post, delete
parameters: none
returns: success message if entity was deleted
```
