<?php
namespace App\Model\Table;

use App\Model\Entity\DicWord;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use PhpParser\Node\Expr\Cast\Array_;

/**
 * DicWords Model
 *
 * @method \App\Model\Entity\DicWord get($primaryKey, $options = [])
 * @method \App\Model\Entity\DicWord newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\DicWord[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\DicWord|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\DicWord patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\DicWord[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\DicWord findOrCreate($search, callable $callback = null)
 */
class DicWordsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('dic_words');
        $this->displayField('id');
        $this->primaryKey('id');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('word', 'create')
            ->notEmpty('word');


        return $validator;
    }

    public function getAnagrams(Array $entities, $dictionaryId = 1)
    {
        $dictionaryEntities = $this->find()->where(['dictionary_id'=>$dictionaryId])->toArray();
        $dictionary = [];

        foreach($entities as $entity){
            $dictionary[$entity->word] = $this->getAnagramsForEntity($entity, $dictionaryEntities, $dictionaryId);
        }

        return $dictionary;
    }

    public function getAnagramsForEntity(DicWord $entity, $dictionaryEntities = null, $dictionaryId = 1)
    {
        if(!$dictionaryEntities){
            $dictionaryEntities = $this->find()->where(['dictionary_id'=>$dictionaryId])->toArray();
        }
        $entityAnagrams = [];
        foreach($dictionaryEntities as $dicEntity){
            if($entity->signatureEquals($dicEntity)){
                $entityAnagrams[] = $dicEntity->word;
            }
        }
        return $entityAnagrams;
    }
}
