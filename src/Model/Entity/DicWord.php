<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * DicWord Entity
 *
 * @property int $id
 * @property string $word
 * @property string $signature
 */
class DicWord extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];

    protected function _setWord($word)
    {
        $wordLetters = str_split($word);
        sort($wordLetters);
        $this->set('signature', implode('',$wordLetters));
        return $word;
    }

    public function signatureEquals(DicWord $entity){
        return strtolower($this->signature) == strtolower($entity->signature);
    }

    public function toString(){
        return $this->word;
    }
}
