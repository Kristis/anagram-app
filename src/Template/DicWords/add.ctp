<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('List Dic Words'), ['action' => 'index']) ?></li>
    </ul>
</nav>
<div class="dicWords form large-9 medium-8 columns content">
    <?= $this->Form->create($dicWord) ?>
    <fieldset>
        <legend><?= __('Add Dic Word') ?></legend>
        <?php
            echo $this->Form->input('word');
            echo $this->Form->input('signature');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
