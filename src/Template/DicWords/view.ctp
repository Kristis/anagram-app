<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Dic Word'), ['action' => 'edit', $dicWord->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Dic Word'), ['action' => 'delete', $dicWord->id], ['confirm' => __('Are you sure you want to delete # {0}?', $dicWord->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Dic Words'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Dic Word'), ['action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="dicWords view large-9 medium-8 columns content">
    <h3><?= h($dicWord->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Word') ?></th>
            <td><?= h($dicWord->word) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Signature') ?></th>
            <td><?= h($dicWord->signature) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($dicWord->id) ?></td>
        </tr>
    </table>
</div>
