<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Dictionary'), ['action' => 'edit', $dictionary->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Dictionary'), ['action' => 'delete', $dictionary->id], ['confirm' => __('Are you sure you want to delete # {0}?', $dictionary->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Dictionaries'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Dictionary'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Dic Words'), ['controller' => 'DicWords', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Dic Word'), ['controller' => 'DicWords', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="dictionaries view large-9 medium-8 columns content">
    <h3><?= h($dictionary->name) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Name') ?></th>
            <td><?= h($dictionary->name) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($dictionary->id) ?></td>
        </tr>
    </table>
    <div class="related">
        <h4><?= __('Related Dic Words') ?></h4>
        <?php if (!empty($dictionary->dic_words)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Word') ?></th>
                <th scope="col"><?= __('Signature') ?></th>
                <th scope="col"><?= __('Dictionary Id') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($dictionary->dic_words as $dicWords): ?>
            <tr>
                <td><?= h($dicWords->id) ?></td>
                <td><?= h($dicWords->word) ?></td>
                <td><?= h($dicWords->signature) ?></td>
                <td><?= h($dicWords->dictionary_id) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'DicWords', 'action' => 'view', $dicWords->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'DicWords', 'action' => 'edit', $dicWords->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'DicWords', 'action' => 'delete', $dicWords->id], ['confirm' => __('Are you sure you want to delete # {0}?', $dicWords->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
</div>
