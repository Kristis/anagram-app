<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('List Dictionaries'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Dic Words'), ['controller' => 'DicWords', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Dic Word'), ['controller' => 'DicWords', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="dictionaries form large-9 medium-8 columns content">
    <?= $this->Form->create($dictionary) ?>
    <fieldset>
        <legend><?= __('Add Dictionary') ?></legend>
        <?php
            echo $this->Form->input('name');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
