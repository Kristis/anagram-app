<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Dictionary'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Dic Words'), ['controller' => 'DicWords', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Dic Word'), ['controller' => 'DicWords', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="dictionaries index large-9 medium-8 columns content">
    <h3><?= __('Dictionaries') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('name') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($dictionaries as $dictionary): ?>
            <tr>
                <td><?= $this->Number->format($dictionary->id) ?></td>
                <td><?= h($dictionary->name) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $dictionary->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $dictionary->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $dictionary->id], ['confirm' => __('Are you sure you want to delete # {0}?', $dictionary->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
