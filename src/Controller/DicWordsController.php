<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * DicWords Controller
 *
 * @property \App\Model\Table\DicWordsTable $DicWords
 */
class DicWordsController extends AppController
{

    public function anagrams(){

        $words = explode(" ",$this->request->query('words'));

        $wordEntities = [];
        foreach($words as $word){
            $wordEntities[] = $this->DicWords->newEntity(['word' => strtolower($word)]);
        }

        $this->set('dictionary',$this->DicWords->getAnagrams($wordEntities,$this->request->params['dictionary_id']));
    }

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $dicWords = $this->DicWords->find()->where(['dictionary_id' => $this->request->params['dictionary_id']])->toArray();
        $this->set('words',$dicWords);
    }

    /**
     * View method
     *
     * @param string|null $id Dic Word id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $dicWord = $this->DicWords->get($id, [
            'contain' => []
        ]);

        $this->set('dicWord', $dicWord);
        $this->set('_serialize', ['dicWord']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $this->request->allowMethod(['post']);
        $dicWord = $this->DicWords->newEntity();
        if($this->DicWords->find()->where(['dictionary_id' => $this->request->params['dictionary_id'], 'word' => $this->request->data['word']])->count() > 0){
            $this->set('error', 'Word already exists in the dictionary');
            return;
        }
        if ($this->request->is('post')) {
            $dicWord->word = $this->request->data['word'];
            $dicWord->dictionary_id = $this->request->params['dictionary_id'];
            if ($this->DicWords->save($dicWord)) {
                $this->Flash->success(__('The dic word has been saved.'));
            } else {
                $this->Flash->error(__('The dic word could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('dicWord'));
        $this->set('_serialize', ['dicWord']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Dic Word id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $this->request->allowMethod(['patch', 'post', 'put']);
        $dicWord = $this->DicWords->get($id, [
            'contain' => []
        ]);
        if($this->DicWords->find()->where(['dictionary_id' => $dicWord->dictionary_id, 'word' => $this->request->data['word']])->count() > 0){
            $this->set('error', 'Word already exists in the dictionary');
            return;
        }
        if ($this->request->is(['patch', 'post', 'put'])) {
            $dicWord = $this->DicWords->patchEntity($dicWord, $this->request->data);
            if ($this->DicWords->save($dicWord)) {
                $this->Flash->success(__('The dic word has been saved.'));
            } else {
                $this->Flash->error(__('The dic word could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('dicWord'));
        $this->set('_serialize', ['dicWord']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Dic Word id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $dicWord = $this->DicWords->get($id);
        if ($this->DicWords->delete($dicWord)) {
            $this->Flash->success(__('The dic word has been deleted.'));
        } else {
            $this->Flash->error(__('The dic word could not be deleted. Please, try again.'));
        }

        $this->set('message','success');
    }
}
