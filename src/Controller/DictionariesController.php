<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Dictionaries Controller
 *
 * @property \App\Model\Table\DictionariesTable $Dictionaries
 */
class DictionariesController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $dictionaries = $this->paginate($this->Dictionaries);

        $this->set(compact('dictionaries'));
        $this->set('_serialize', ['dictionaries']);
    }

    /**
     * View method
     *
     * @param string|null $id Dictionary id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $dictionary = $this->Dictionaries->get($id);

        $this->set('dictionary', $dictionary);
        $this->set('_serialize', ['dictionary']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $this->request->allowMethod(['post']);
        $dictionary = $this->Dictionaries->newEntity();
        if ($this->request->is('post')) {
            $dictionary = $this->Dictionaries->patchEntity($dictionary, $this->request->data);
            if ($this->Dictionaries->save($dictionary)) {
                $this->Flash->success(__('The dictionary has been saved.'));
            } else {
                $this->Flash->error(__('The dictionary could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('dictionary'));
        $this->set('_serialize', ['dictionary']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Dictionary id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $this->request->allowMethod(['patch', 'post', 'put']);
        $dictionary = $this->Dictionaries->get($id, [
            'contain' => []
        ]);

        if ($this->request->is(['patch', 'post', 'put'])) {
            $dictionary = $this->Dictionaries->patchEntity($dictionary, $this->request->data);
            if ($this->Dictionaries->save($dictionary)) {
                $this->Flash->success(__('The dictionary has been saved.'));
            } else {
                $this->Flash->error(__('The dictionary could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('dictionary'));
        $this->set('_serialize', ['dictionary']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Dictionary id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $dictionary = $this->Dictionaries->get($id);
        if ($this->Dictionaries->delete($dictionary)) {
            $this->Flash->success(__('The dictionary has been deleted.'));
        } else {
            $this->Flash->error(__('The dictionary could not be deleted. Please, try again.'));
        }

        $this->set('message','success');
    }
}
